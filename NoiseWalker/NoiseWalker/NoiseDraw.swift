//
//  NoiseDraw.swift
//  NoiseWalker
//
//  Created by student on 9/14/18.
//  Copyright © 2018 ASU. All rights reserved.
//

import Foundation
import Tin

class NoiseDraw {
    var amplitude: Double
    var scale: Double

    init(amplitude: Double, scale: Double) {
        self.amplitude = amplitude
        self.scale = scale
    }
    
    var offset = 0.0
    var v = 0.0
    
    func draw() {
        var x = 0.0
        
        while x < tin.width {
            
            let u = x * scale + offset
            let dy = noise(x: u, y: v) * amplitude
            
            
            /*let wavelength = x * scale
             
             let dy1 = turbulence(x: x, length: scale, amplitude: amplitude, octave: 1, offset: offset)
             let dy2 = turbulence(x: x, length: scale, amplitude: amplitude, octave: 2, offset: offset)
             */
            
            let y = 300.0 + dy
            
            ellipse(centerX: x, centerY: y, width: 10.0, height: 10.0)
            x += 2
        }
        offset = offset + 1.0
        v = v + 0.01
    }
    
}
