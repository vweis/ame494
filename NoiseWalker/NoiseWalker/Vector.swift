//
//  Vector.swift
//  NoiseWalker
//
//  Created by student on 9/12/18.
//  Copyright © 2018 ASU. All rights reserved.
//

import Foundation
import Tin

struct Force {
    var angle: Double
    var magnitude: Double
    
    init(angle: Double, magnitude: Double) {
        self.angle = angle
        self.magnitude = magnitude
        
    }
}
