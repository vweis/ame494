//
//  DrunkWalker.swift
//  NoiseWalker
//
//  Created by student on 9/12/18.
//  Copyright © 2018 ASU. All rights reserved.
//

import Foundation
import Tin

class DrunkWalker {
    var position: TVector2
    var velocity: TVector2
    var radius: Double
    var maxSpeed: Double = 6
    
    init(x: Double, y: Double) {
        position = TVector2(x: x, y: y)
        velocity = TVector2(angle: 0.3 * Double.pi)
        velocity.magnitude = 3
        radius = 30.0
    }
    
    

    
    var offset = 0.0
    //var v = 0.0
    let scale = 0.01
    func update() {
        let u = velocity.x * scale + offset
        let v = velocity.y * scale + offset
        let influence = noise(x: u, y: v) * 9
        let alcohol = TVector2(x: influence, y: influence)
        velocity = velocity + alcohol
        velocity.magnitude = maxSpeed/1.3
        velocity.limit(mag: maxSpeed)
        position = position + velocity
        
        //v += 0.01
        offset += 1.0
        if offset == 100.0 {
            offset = 0.0
        }
    }
    
    func checkEdges() {
        if position.x + radius > tin.width {
            //right side
            velocity.x = velocity.x * -1.0
            position.x = tin.width - radius
        }
        else if position.x - radius < 0 {
            //left side
            velocity.x = velocity.x * -1.0
            position.x = radius
        }
        if position.y + radius > tin.height {
            //top
            velocity.y = velocity.y * -1.0
            position.y = tin.height - radius
        }
        else if position.y - radius < 0 {
            //bottom
            velocity.y = velocity.y * -1.0
            position.y = radius
        }
    }
    
    
    func display() {
        checkEdges()
        strokeDisable()
        fillColor(gray: 0.85)
        ellipse(centerX: position.x, centerY: position.y, width: radius * 2, height: radius * 2)
    }
    
    func run() {
        
        update()
        display()
        
    }
}
