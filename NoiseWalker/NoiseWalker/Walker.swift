//
//  Walker.swift
//  Walker
//
//

import Foundation
import Tin


class Walker {
    var position: TVector2

    var shapeWidth: Double
    var shapeHeight: Double
    var speedX: Double
    var speedY: Double
    var speedMin: Double
    var speedMax: Double
    
    init() {
        position = TVector2(x: 0, y: 0)
        position.x = tin.midX
        position.y = tin.midY
        shapeWidth = 20.0
        shapeHeight = 20.0
        speedX = 1.0
        speedY = 2.0
        speedMin = 0.5
        speedMax = 1.5
    }
    
    
    func step() {
        
        
        //Probability that the direction of X is leftward is ~66%, rightward is ~33%.
        let speedXPolarity = Int(random(max:1.5))
        
        switch speedXPolarity {
        case 0:
            speedX = random(min: speedMin, max: speedMax)
        case 1:
            speedX = random(min: speedMin, max: speedMax) * -1.0
        default:
            speedX = random(min: speedMin, max: speedMax)
        }
        
        
        //Probability that the direction of Y is upward is 50%, downward is 50%.
        let speedYPolarity = Int(random(max:2.0))
        switch speedYPolarity {
        case 0:
            speedY = random(min: speedMin, max: speedMax)
        case 1:
            speedY = random(min: speedMin, max: speedMax) * -1.0
        default:
            speedY = random(min: speedMin, max: speedMax)
        }
        
        
        let choice = Int(random(max: 5.0))
         /*if choice == 0 {x += speed}|if choice == 1 {y += speed}|if choice == 2 {x -= speed}|if choice == 3 {y -= speed}*/
        
        
        /*Here, the directions are in part determined by the polarity of the speed variable assigned to each axis, so each case represents more than one outcome. I'll refer to directions in terms of compass directions for the sake of clarity.
         //Probability of the square moving west is ~26.6%.
         //Probability of the square moving northwest is ~6.6%.
         //Probability of the square moving north is 20%.
         //Probability of the square moving northeast is ~3.3%.
         //Probability of the square moving east is ~13.3%.
         //Probability of the square moving southeast is ~3.3%.
         //Probability of the square moving south is 20%.
         //Probability of the square moving southwest is ~6.6%.
 */
        switch choice {
        case 0:
            position.x += speedX
        case 1:
            position.y += speedY
        case 2:
            position.x += speedX
        case 3:
            position.y += speedY
        case 4:
            position.x += speedX
            position.y += speedY
        default:
            print("Choice switch failure.")
        }
 
        enum Direction: Int {
            case up, right, down, left /*diagonal*/
        }
        /*func decide() -> Direction {
            var decision: Int
            //decision = Int(random(max: 4))
            var position = (x,y)
            switch position {
            case (0.0...800.0, 0.0...400.0):
                return
            default:
                return Direction(rawValue: decision) ?? Direction(rawValue: 0)!
            }
            */
        
        position.x = constrain(value: position.x, min: 0.0, max: tin.width)
        position.y = constrain(value: position.y, min: 0.0, max: tin.height)
    }
    
    
    func display() {
        strokeColor(gray: 0.0)
        rect(x: position.x, y: position.y, width: 20.0, height: 20.0)
    }
}
