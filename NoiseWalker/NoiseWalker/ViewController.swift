//
//  ViewController.swift
//  Walker
//
//

import Cocoa
import Tin


class ViewController: TController {

    override func viewWillAppear() {
        view.window?.title = "Random Walker with Perlin Noise"
        
        makeView(width: 800.0, height: 400.0)
        
        let scene = Scene()
        
        present(scene: scene)
    }

}


class Scene: TScene {
    
    var drunk = DrunkWalker(x: 1, y: 1)
    var noisyThing = NoiseWalker(x: 0, y: 0)
    var noise = NoiseDraw(amplitude: 100.0, scale: 0.01)

    var i = 0
    override func setup() {
        tin.enableRestoreFromPrevious()
        noisyThing.generateNoises(quantity: 20)
    }
    
    override func update() {
        if tin.frameCount <= 2 {
            background(gray: 0.5)
        }
        
        let slowTimer = 3
        if i < slowTimer {
            i += 1
        }
        if i == slowTimer {
            i = 0
            background(gray: 0.5)
            //noise.draw()
            print(drunk.position)
            /*for _ in noisyThing.noises {
            noisyThing.run()
            }*/
        }
        
        drunk.run()
    }
}

