//
//  NoiseWalker.swift
//  NoiseWalker
//
//  Created by student on 9/14/18.
//  Copyright © 2018 ASU. All rights reserved.
//

import Foundation
import Tin

class NoiseWalker {
    var position: TVector2
    var velocity: TVector2
    var acceleration: TVector2
    var radius: Double
    var maxSpeed: Double
    var noises: [NoiseWalker]
    
    init(x: Double, y: Double) {
        position = TVector2(x: x, y: y)
        velocity = TVector2(x: 0, y: 0)
        acceleration = TVector2(x: 0.001, y: 0.001)
        radius = 30.0
        maxSpeed = 1.0
        noises = []
    }
    
    
    func generateNoises(quantity: Int) {
        for _ in 1...quantity {
            let n = NoiseWalker(x: random(min: 0.0, max: tin.width), y: random(min: 0, max: tin.height))
            noises.append(n)
        }
    }
    
    
    
    func update() {
        /*acceleration = TVector2(x: tin.mouseX - position.x, y: tin.mouseY - position.y)
         acceleration.normalize()
         acceleration = acceleration * 6.0 */
        
        
        /*let ax = noise(x: 0.0, y: 0.0)
         let ay = noise(x: 0.0, y: 0.0)
         acceleration = TVector2(x: ax, y: ay)*/
        
        /*let u = position.x * 0.01
         let v = position.y * 0.01*/
        let u = 0.01
        let v = -0.01
        
        let theta = noise(x: u, y: v) * Double.pi
        
        
        acceleration = TVector2(angle: theta)
        acceleration.magnitude = 0.3
        
        
        
        velocity = velocity + acceleration
        velocity.limit(mag: maxSpeed)
        position = position + velocity
        
        
    }
    
    func checkEdges() {
        if position.x + radius > tin.width {
            //right side
            velocity.x = velocity.x * -1.0
            position.x = tin.width - radius
        }
        else if position.x - radius < 0 {
            //left side
            velocity.x = velocity.x * -1.0
            position.x = radius
        }
        if position.y + radius > tin.height {
            //top
            velocity.y = velocity.y * -1.0
            position.y = tin.height - radius
        }
        else if position.y - radius < 0 {
            //bottom
            velocity.y = velocity.y * -1.0
            position.y = radius
        }
    }
    
    
    func display() {
        checkEdges()
        strokeDisable()
        fillColor(gray: 0.3)
        ellipse(centerX: position.x, centerY: position.y, width: radius * 2, height: radius * 2)
    }
    
    func run() {
        update()
        display()
    }
}
