//
//  NoiseTest.swift
//  NoiseWalker
//
//  Created by student on 9/14/18.
//  Copyright © 2018 ASU. All rights reserved.
//

import Foundation
import Tin

class Noise {

    
    init() {

    }
    var x: Double
    func draw() {
        for i in 1...Int(tin.width) {
            let x = Double(i)
            let y = noise(x: 0.0, y: 0.0)
            ellipse(centerX: x, centerY: y, width: 1, height: 1)
        }
    }
}
