//
//  ViewController.swift

//

import Cocoa
import Tin


class ViewController: TController {

    var scene: Scene!
    
    override func viewWillAppear() {
        super.viewWillAppear()
        view.window?.title = "Agents"
        makeView(width: 1200.0, height: 800.0)
        scene = Scene()
        present(scene: scene)
        scene.view?.showStats = false
    }
    
    override func mouseDragged(with event: NSEvent) {
        super.mouseDragged(with: event)
        scene.moveTarget(x: tin.mouseX, y: tin.mouseY)
    }
    
}


class Scene: TScene {
    
    
    var vehicles: [Vehicle] = []
    var env = Environment()
    
    
    override func setup() {
        for _ in 1...10 {
            let v = Vehicle(x: random(max: tin.width), y: random(max: tin.height))
            vehicles.append(v)
        }
    }
    
    
    override func update() {
        background(gray: 1.0)
        
        env.render()
        env.moreFood()
        
        for v in vehicles {
            v.run(env: env)
            v.wander()
        }
    }
    
    func moveTarget(x: Double, y: Double) {
        env.predator = TVector2(x: x, y: y)
    }
    
}

