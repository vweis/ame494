//
//  Environment.swift
//  Agents
//
//

import Foundation
import Tin


class Environment {
    
    var food: [TVector2]
    var predator = TVector2(x: random(min: 0, max: tin.width), y: random(min: 0, max: tin.height))
    
    init() {
        food = []
        
        //add an initial 50 instances of food
        for _ in 1...50 {
            let x = random(max: tin.width)
            let y = random(max: tin.height)
            addFood(x: x, y: y)
        }
    }
    
    func moreFood() {
        if food.count < 18 {
            addFood(x: random(max: tin.width), y: random(max: tin.width))
        }
    }
    
    
    func addFood(x: Double, y: Double) {
        let f = TVector2(x: x, y: y)
        food.append(f)
    }
    
    
    func render() {
        pushState()
        fillColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 0.5)
        ellipse(centerX: predator.x, centerY: predator.y, width: 20, height: 20)
        popState()
        
        for f in food {
            strokeDisable()
            fillColor(red: 0.1, green: 0.8, blue: 0.1, alpha: 1)
            ellipse(centerX: f.x, centerY: f.y, width: 8, height: 8)
        }
    }
    
}
