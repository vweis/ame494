//
//  LSystem.swift
//  Plants
//  Animating Virtual Worlds
//

import Foundation
import Tin


class LSystem {
    var sentence: String        // The sentence (a String)
    var ruleset: [Rule]         // The ruleset (an array of Rule objects)
    var generation: Int         // Keeping track of the generation #
    
    
    init(axiom: String, rules: [Rule]) {
        sentence = axiom
        ruleset = rules
        generation = 0
    }
    
    
    // Generate the next generation
    func generate() {
        // nextgen is the output - the next generation.
        var nextgen = ""
        
        // For every character in the sentence
        for c in sentence {
            // turn the current character into a string
            let curr = "\(c)"
            
            // We will replace it with itself unless it matches one of our rules
            var replace = "\(c)"
            if let rule = Rule.select(ruleset: ruleset, char: curr) {
                replace = rule.successor
            }
            
            // append replace to the end of next generation
            nextgen.append(replace)
        }
        
        // Replace sentence property with next generation
        sentence = nextgen
        
        // Increment generation
        generation += 1
    }
    
    
    // compute next generations
    func advance(generations: Int) {
        for i in 1...generations {
            print("generate \(i)")
            generate()
        }
    }
    

}
