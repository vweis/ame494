//
//  Turtle.swift
//  Plants
//  Animating Virtual Worlds
//
//

import Foundation
import Tin


class Turtle {

    var color = [0.0,0.0,0.0,0.0]
    var initColor = [0.0,0.0,0.0,0.0]
    var finalColor = [0.0,0.0,0.0,0.0]
    var colorDirection = [true,true,true,true]
    
    init(initialColor: [Double], finalColor: [Double]) {
        initColor = initialColor
        self.finalColor = finalColor
        color = initColor
    }
    
    func render(sentence: String, length: Double, theta: Double) {
        for c in sentence {
            if c == "F" || c == "R" || c == "L" {
                // Draw a transparent line down x axis,
                // then translate down the length of the line.
                strokeColor(red: color[0], green: color[1], blue: color[2], alpha: color[3])
                line(x1: 0, y1: 0, x2: length, y2: 0)
                translate(dx: length, dy: 0)
            }
            else if c == "G" {
                // Translate an ammount length and reestablish a base. (No drawing.)
                translate(dx: length, dy: 0)
                strokeDisable()
                fillColor(red: color[0], green: color[1], blue: color[2], alpha: color[3])
                ellipse(centerX: 0, centerY: 0, width: length/3, height: length/3)
                strokeEnable()
            }
            else if c == "+" {
                // Rotate clockwise
                rotate(by: -theta)
            }
            else if c == "-" {
                // Rotate counter-clockwise
                rotate(by: theta)
            }
            else if c == "[" {
                // Save current graphics state.
                // This is useful to create a branch.
                pushState()
            }
            else if c == "]" {
                // Restore current graphics state.
                // This is useful to end a branch.
                popState()
            }
            else if c == "A" {
                //
                for i in 0...(color.count-1) {
                    
                    incrementColor(i: i)
                    if color[i] <= 0.0 || color[i] >= 1.0 {
                        colorDirection[i].toggle()
                        incrementColor(i: i)
                    }
                }
                print(color)
            }
            else if c == "B" {
                //
                color = initColor
                strokeDisable()
                fillColor(red: color[0], green: color[1], blue: color[2], alpha: color[3])
                ellipse(centerX: 0, centerY: 0, width: length/3, height: length/3)
                strokeEnable()
            }
        }
    }
    
    func incrementColor(i: Int) {
        let difference = (finalColor[i] - initColor[i])/80.0
        if colorDirection[i] == true {
            color[i] = color[i] + difference
        } else if colorDirection[i] == false {
            color[i] = color[i] - difference
        }
        print("Difference for color \(i) is \(difference)")
    }
    
}
