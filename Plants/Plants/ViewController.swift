//
//  ViewController.swift
//  Animating Virtual Worlds

import Cocoa
import Tin



class ViewController: TController {

    var scene: Scene!
    
    override func viewWillAppear() {
        super.viewWillAppear()
        view.window?.title = "Plants"
        makeView(width: 1200.0, height: 800.0)
        scene = Scene()
        present(scene: scene)
        scene.view?.showStats = false
    }
    
    
    override func mouseUp(with event: NSEvent) {
        scene.mousePressed()
    }

}


class Scene: TScene {
    
    var plant1: LSystem!
    var plant2: LSystem!
    var turtle: Turtle!
    
    
    override func setup() {
        // Create one simple L-system, it only has one rule.
        let rule1 = Rule(predecessor: "F", successor: "FF[+F]A", probability: 0.20)
        let rule2 = Rule(predecessor: "F", successor: "FF[-F]A", probability: 0.20)
        let rule3 = Rule(predecessor: "F", successor: "F[++F]A", probability: 0.30)
        let rule4 = Rule(predecessor: "F", successor: "F[--F]A", probability: 0.20)
        let rule5 = Rule(predecessor: "F", successor: "F[+F][-F]GA", probability: 0.10)
        let ruleset = [rule1, rule2, rule3, rule4, rule5]
        
        plant1 = LSystem(axiom: "BF", rules: ruleset)
        plant2 = LSystem(axiom: "BFF", rules: ruleset)
        // The drawing object.
        turtle = Turtle(initialColor: [0.1, 1.0, 0.1, 0.87], finalColor: [1.0, 0.1, 0.0, 0.38])
        
        // grow the plant 5 generations
        plant1.advance(generations: 9)
        plant2.advance(generations: 8)

    }
    
    
    override func update() {

        drawPlants()
        view?.stopUpdates()
    }
    
    
    
    
    
    func mousePressed() {
        //print("generate")
        //a.generate()
        
        drawPlants()
        
    }
    
    func drawPlants() {
        view?.needsDisplay=true
        background(gray: 0.5)
        
        // ***************************************************************
        // Draw one plant
        pushState()
        
        // Translate to where we want the base of the plant.
        translate(dx: tin.midX, dy: tin.midY/2)
        // Rotate counterclockwise 90 degrees, so the plant grows in positive Y direction.
        rotate(by: .pi/2.0)
        
        // Use the sentence from L-system plant1 to provide the drawing instructions.
        turtle.render(sentence: plant1.sentence, length: 45.0, theta: .pi/4)
        
        
        
        popState()
        
        pushState()
        translate(dx: tin.midX, dy: tin.midY/2)
        rotate(by: .pi/2.0)
        turtle.render(sentence: plant2.sentence, length: 90.0, theta: .pi/2)
        popState()
        // Finished with one plant
        // ***************************************************************
        view?.needsDisplay = false
    }
    
    
}

