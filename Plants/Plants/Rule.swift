//
//  Rule.swift
//  Plants
//  Animating Virtual Worlds
//

import Foundation
import Tin


class Rule {
    var predecessor: String
    var successor: String
    var probability: Double
    
    init(predecessor: String, successor: String, probability: Double) {
        self.predecessor = predecessor
        self.successor = successor
        self.probability = probability
    }
    
    
    convenience init(a: String, b: String) {
        self.init(predecessor: a, successor: b, probability: 1.0)
    }
    
    
    // A class method. finds, and returns the rule which applies to the char.
    // Probability provides opportunity for stochastic rulesets.
    static func select(ruleset: [Rule], char: String) -> Rule? {
        var result: Rule? = nil
        var matches: [Rule] = []
        for rule in ruleset {
            if rule.predecessor == char {
                matches.append(rule)
            }
        }
        if matches.count > 0 {
            let choice = random(max: 1)
            var sum = 0.0
            for rule in matches {
                sum += rule.probability
                if choice <= sum {
                    result = rule
                    break
                }
            }
        }
        return result
    }
}
