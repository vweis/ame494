//
//  ViewController.swift
//  Drawing
//


import Cocoa
import Tin


class ViewController: TController {

    var scene: Scene!
    
    override func viewWillAppear() {
        view.window?.title = "Drawing"
        makeView(width: 800.0, height: 600.0)
        scene = Scene()
        present(scene: scene)
    }

}


class Scene: TScene {
    
    
    override func update() {
        background(gray: 0.5)
        
        let amplitude = 100.0
        let scale = 0.01
        
        
        strokeDisable()
        var x = 0.0
        var offset = 0.0
        var v = 0.0
        while x < 800.0 {
            
            let u = x * scale + offset
            let dy = noise(x: u, y: v) * amplitude

            
            let wavelength = x * scale
            
            let dy1 = turbulence(x: x, length: scale, amplitude: amplitude, octave: 1, offset: offset)
            let dy2 = turbulence(x: x, length: scale, amplitude: amplitude, octave: 2, offset: offset)
            
            let y = 300.0 + dy
            
            ellipse(centerX: x, centerY: y, width: 10.0, height: 10.0)
            x += 4
        }
        

        offset = offset + 1.0
        v = v + 0.01
        //view?.stopUpdates()
    }
    
}

func turbulence(x: Double, length: Double, amplitude: Double, octave: Double, offset: Double) -> Double {
    let thing = pow(2.0, octave - 1.0)
    let o_wavelength = length * thing
    let o_amplitude = amplitude / thing
    let u = x * o_wavelength + offset
    return noise(x: u, y: 0.0) * o_amplitude
}
