//
//  ViewController.swift
//  Projectile
//
//

import Cocoa
import Tin


class ViewController: TController {
    var scene: Scene!
    
    override func viewWillAppear() {
        view.window?.title = "Projectile Motion"
        
        makeView(width: 1200.0, height: 800.0)
        
        scene = Scene()
        present(scene: scene)
        scene.view?.showStats = false
    }
    
    
    override func keyUp(with event: NSEvent) {
        if let keys = event.charactersIgnoringModifiers {
            if keys == " " {
                // Launch a projectile
                scene.launch()
            }
            else if keys == "w" {
                // Rotate launcher up
                scene.rotateLauncher(delta: 0.05)
            }
            else if keys == "s" {
                // Rotate launcher down
                scene.rotateLauncher(delta: -0.05)
            }
            else if keys == "d" {
                // Increase launch magnitude
                scene.scaleLauncherMagnitude(scale: 1.05)
            }
            else if keys == "a" {
                // Decrease launch magnitude
                scene.scaleLauncherMagnitude(scale: 0.95)
            }
            else if keys == "r" {
                // Reset the scenario
                scene.reset()
            }
        }
    }
    
    override func mouseUp(with event: NSEvent) {
        scene.createSystem(x: tin.mouseX, y: tin.mouseY)
    }
    
}



class Scene: TScene {
    
    // Properties of the Scene go here.
    // They will persist as long as your program is running.
    
    var systems: [ParticleSystem] = []
    
    var projectiles: [Mover] = []
    
    //var projectile = Mover()
    
    var barrier = Box(x: 450.0, y: 0.0, width: 200.0, height: 400.0)
    
    var target = Box(x: tin.width - 70, y: 0, width: 30, height: 40)
    
    let launcherOrigin = TVector2(x: 20.0, y: 20.0)
    //var launcherDirect: TVector2 //unit vector for angle
    var launchForce = TVector2(x: 8.0, y: 5.0)
    var gravity = TVector2(x: 0, y: -0.065)
    
    var wind = TVector2(x: -0.1, y: 0)
    var v = 0.0
    var offset = 0.0
    
    
    
    override func setup() {
        reset()
        

        //projectile.position = TVector2(x: 200.0, y: 200.0)
    }
    //DispatchQueue.global().async {}
    override func update() {
        
        
        
        background(gray: 0.5)
        
        // Draw the barrier
        
        barrier.render()
        // Draw the target
        target.render()
        
        // Draw an indicator for the wind direction and magnitude.
        // You will need to draw something scaled to be visible to the user.
        // eg. the actual wind force will have a magnitude too small for
        // a reasonable display.
        arrow(x1: tin.midX, y1: tin.height - 30, x2: tin.midX + (wind.x * 2000), y2: tin.height - 30)
        
        // Update and display any projectiles
        
        
        for projectile in projectiles {
        cleanProjectiles(projectile: projectile)
        checkEdges(projectile: projectile, barrier: barrier)
        checkEdges(projectile: projectile, barrier: target)
            if target.intersect(mover: projectile) {
                target.fill(red: 0, green: 90, blue: 0)
            }
        projectile.applyForce(force: gravity)
        projectile.applyForce(force: wind)
        projectile.update()
        
        projectile.render()
        
        
        
        }
        
        for ps in systems {
            
        ps.run()
        ps.addParticle()
        }
        // Draw the Launcher
        drawLauncher()
        

    }
    
    func createSystem(x: Double, y: Double) {
        let ps = ParticleSystem(position: TVector2(x: x, y: y))
        systems.insert(ps, at: 0)
    }
    
    func checkEdges(projectile: Mover, barrier: Box) -> Bool {
        if barrier.intersect(mover: projectile) {
            
            let topOfBall = projectile.position.y + projectile.radius
            let leftOfBall = projectile.position.x - projectile.radius
            let rightOfBall = projectile.position.x + projectile.radius
            let bottomOfBall = projectile.position.y - projectile.radius
            
            let topOfWall = barrier.position.y + barrier.height
            let leftOfWall = barrier.position.x
            let rightOfWall = barrier.position.x + barrier.width
            let bottomOfWall = barrier.position.y
            
            
            //top edge
            if (bottomOfBall <= topOfWall) && (rightOfBall >= leftOfWall) && (leftOfBall <= rightOfWall) && (topOfBall >= topOfWall) {
                projectile.velocity.y = projectile.velocity.y * -1
            }
            else if (rightOfBall >= leftOfWall) && (bottomOfBall <= topOfWall) && (topOfBall >= bottomOfWall){
                //right edge
                if (leftOfBall <= rightOfWall) && (leftOfBall >= leftOfWall){
                    projectile.velocity.x = projectile.velocity.x * -1
                }
                    //left edge
                else if (rightOfBall <= rightOfWall) && (leftOfBall <= leftOfWall) {
                    projectile.velocity.x = projectile.velocity.x * -1
                }
            }
        }
        return barrier.intersect(mover: projectile)
    }
    
    func cleanProjectiles(projectile: Mover) {
        let topOfBall = projectile.position.y + projectile.radius
        let leftOfBall = projectile.position.x - projectile.radius
        let rightOfBall = projectile.position.x + projectile.radius
        let bottomOfBall = projectile.position.y - projectile.radius
        
        if (leftOfBall >= tin.width) {
            projectiles = projectiles.filter { $0 != projectile }
        }
        if (topOfBall <= 0) {
            projectiles = projectiles.filter { $0 != projectile }
        }
        
    }
    
    
    
    
    
    // Create a new projectile object.
    // Launch the new project by applying a launch force to the object.
    func launch() {
        let projectileGen = Mover()
        projectileGen.applyForce(force: launchForce)
        projectiles.append(projectileGen)
        
    }
    
    
    // Draw the launcher object
    func drawLauncher() {
        //let pt = TVector2(x: 200.0, y: 200.0)
        
        pushState()
        rotate(by: launchForce.heading())
        fillColor(red: 20, green: 20, blue: 20, alpha: 1.0)
        rect(x: 0, y: -10, width: 100, height: 20)
        
        arrow(x1: 100, y1: 10, x2: 100 + (8 * launchForce.magnitude), y2: 10)
        popState()
        
    }
    
    
    // Rotate launcher
    // delta is an amount to rotate in radians.
    func rotateLauncher(delta: Double) {
        launchForce.rotate(theta: delta)
        
    }
    
    
    // Scale the launcher magnitude.
    // Greater magnitude will shoot projectile further.
    func scaleLauncherMagnitude(scale: Double) {
        launchForce.magnitude = launchForce.magnitude * scale
        
    }

    
    // reset
    // - new random position for the target.
    // - new random position and size for the barrier object
    // - new random direction and magnitude for wind force.
    func reset() {
        let amplitude = 0.15
        let scale = 0.1
        let u = wind.x * scale + offset
        let delta = noise(x: u, y: v)
        
        let dw = delta * amplitude
        
        let barrierPosition = 350 + (delta)*100
        let barrierWidth = 170 + (delta)*250
        let barrierHeight = 200 + (delta)*700
        
        let targetPosition = tin.width - target.width - (delta*70)
        //let dy1 = turbulence(x: x, length: scale, amplitude: amplitude, octave: 1, offset: offset)
        //let dy2 = turbulence(x: x, length: scale, amplitude: amplitude, octave: 2, offset: offset)
        
        barrier = Box(x: barrierPosition, y: 0, width: barrierWidth, height: barrierHeight)
        
        target.position.x = targetPosition
        
        wind.x = dw
        
        offset = offset + 1.0
        v = v + 0.01
        
        
        
        
        
    }
    
    

    
}

