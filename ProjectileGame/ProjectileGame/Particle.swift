//
//  Particle.swift
//  ProjectileGame
//
//  Created by student on 10/3/18.
//  Copyright © 2018 ASU. All rights reserved.
//

import Foundation
import Tin

class Particle: Equatable {
    var position: TVector2
    var velocity: TVector2
    var acceleration: TVector2
    var topspeed: Double
    var mass: Double
    var radius: Double
    var width: Double
    var lifespan: Double
    
    // default initializer - give each property an initial value.
    init(location: TVector2) {
        position = TVector2(x: location.x, y: location.y)
        velocity = TVector2(x: random(min: -1.0, max: 1.0), y: random(min: 0.0, max: 1.0))
        acceleration = TVector2(x: 0.0, y: -0.1)
        topspeed = 50.0
        width = 12.0
        radius = width / 2.0
        mass = 1.0
        lifespan = 255.0
    }
    
    func isDead() -> Bool {
        if lifespan < 0.0 {
            return true
        } else {
            return false
        }
    }
    // update - move the object
    func update() {
        velocity = velocity + acceleration
        velocity.limit(mag: topspeed)
        position = position + velocity
        
        acceleration = acceleration * 0.0
        lifespan = lifespan - 4.0
    }
    
    
    // apply a force vector to acceleration. The value is accumulated,
    // so that multiple forces can be applied during one frame.
    func applyForce(force: TVector2) {
        let f = force / mass
        acceleration = acceleration + f
    }
    
    
    // draw the object
    func render() {
        let a = remap(value: lifespan, start1: 0.0, stop1: 255.0, start2: 0.0, stop2: 1.0)
        strokeColor(gray: 0.0, alpha: a)
        lineWidth(1.0)
        fillColor(gray: 0.9, alpha: a)
        ellipse(centerX: position.x, centerY: position.y, width: width, height: width)
    }
    
    
    
    // This operator method makes the Mover "Equatable"
    // https://developer.apple.com/documentation/swift/equatable
    // Using instance identity, which may not always be a good idea,
    // but it works for this use case.
    static func == (lhs: Particle, rhs: Particle) -> Bool {
        if lhs === rhs {
            return true
        }
        else {
            return false
        }
    }
    
}
