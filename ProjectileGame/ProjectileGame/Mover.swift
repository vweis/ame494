//
//  Mover.swift
//  Motion101
//
// The Nature of Code
// Daniel Shiffman
// http://natureofcode.com
//

import Foundation
import Tin


class Mover: Equatable {
    var position: TVector2
    var velocity: TVector2
    var acceleration: TVector2
    var topspeed: Double
    var mass: Double
    var radius: Double
    var width: Double
    var active: Bool
    
    // default initializer - give each property an initial value.
    init() {
        position = TVector2(x: 20.0, y: 20.0)
        velocity = TVector2(x: 0.0, y: 0.0)
        acceleration = TVector2(x: 0.0, y: 0.0)
        topspeed = 50.0
        width = 20.0
        radius = width / 2.0
        mass = 1.0
        active = true
    }
    
    
    // update - move the object
    func update() {        
        velocity = velocity + acceleration
        velocity.limit(mag: topspeed)
        position = position + velocity
        
        acceleration = acceleration * 0.0
    }
    
    
    // apply a force vector to acceleration. The value is accumulated,
    // so that multiple forces can be applied during one frame.
    func applyForce(force: TVector2) {
        let f = force / mass
        acceleration = acceleration + f
    }
    
    
    // draw the object
    func render() {
        strokeColor(gray: 0.0)
        lineWidth(1.0)
        fillColor(gray: 0.9)
        ellipse(centerX: position.x, centerY: position.y, width: width, height: width)
    }
    
    
    
    // This operator method makes the Mover "Equatable"
    // https://developer.apple.com/documentation/swift/equatable
    // Using instance identity, which may not always be a good idea,
    // but it works for this use case.
    static func == (lhs: Mover, rhs: Mover) -> Bool {
        if lhs === rhs {
            return true
        }
        else {
            return false
        }
    }
    
}
