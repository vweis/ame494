//
//  Box.swift
//  Projectile
//
//

import Foundation
import Tin


class Box {
    var position: TVector2
    var mass: Double
    var width: Double
    var height: Double
    var fillR: Double
    var fillG: Double
    var fillB: Double
    
    
    init(x: Double, y:Double, width: Double, height: Double) {
        position = TVector2(x: x, y: y)
        mass = 1.0
        self.width = width
        self.height = height
        fillR = 1.0
        fillG = 1.0
        fillB = 1.0
    }
    
    func render() {
        strokeColor(gray: 0.0)
        lineWidth(3.0)
        fillColor(red: fillR, green: fillG, blue: fillB, alpha: 1.0)
        rect(x: position.x, y: position.y, width: width, height: height)
    }
    
    
    func clamp(x: Double, a: Double, b: Double) -> Double {
        return min( max(x, a), b )
    }
    
    
    // return true if the mover object intersects with this Box.
    func intersect(mover: Mover) -> Bool {
        let xp = clamp(x: mover.position.x, a: position.x, b: position.x + width)
        let yp = clamp(x: mover.position.y, a: position.y, b: position.y + height)
        let d = dist(x1: mover.position.x, y1: mover.position.y, x2: xp, y2: yp)
        if d <= mover.radius {
            return true
        }
        else {
            return false
        }
    }
    
    
    func fill(red: Double, green: Double, blue: Double) {
        fillR = red
        fillG = green
        fillB = blue
    }
    
}
