//
//  Arrow.swift
//  Projectile
//
//

import Foundation
import Tin


var _arrowSize = 5.0

func arrowSize(_ newValue: Double) {
    _arrowSize = newValue
}


func arrow(x1: Double, y1: Double, x2: Double, y2: Double) {
    line(x1: x1, y1: y1, x2: x2, y2: y2)
    
    let v = TVector2(x: x2 - x1, y: y2 - y1)
    let theta = v.heading()
    
    pushState()
    translate(dx: x2, dy: y2)
    rotate(by: theta)
    line(x1: -_arrowSize, y1: -_arrowSize, x2: 0.0, y2: 0.0)
    line(x1: 0.0, y1: 0.0, x2: -_arrowSize, y2: _arrowSize)
    popState()
}
