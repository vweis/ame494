//
//  ParticleSystem.swift
//  ProjectileGame
//
//  Created by student on 10/10/18.
//  Copyright © 2018 ASU. All rights reserved.
//

import Foundation
import Tin

class ParticleSystem {
    var particles: [Particle]
    var origin: TVector2
    
    init(position: TVector2) {
        self.origin = TVector2(x: position.x, y: position.y)
        self.particles = []
    }
    
    func addParticle() {
        let p = Particle(location: origin)
        particles.insert(p, at: 0)
    }
    
    func run() {
        for particle in particles {
            particle.update()
            particle.render()
            
            if particle.isDead() == true {
                if let index = particles.firstIndex(of: particle) {
                    particles.remove(at: index)
                }
            }
        }
    }
}
