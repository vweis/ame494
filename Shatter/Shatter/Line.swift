//
//  Line.swift
//  Shatter
//
//  Created by student on 10/17/18.
//  Copyright © 2018 ASU. All rights reserved.
//

import Foundation
import Tin

class Line: Rectangle {
    var length: Double
    var theta: Double
    init(x: Double, y: Double, l: Double, angle: Double) {
        length = l
        theta = angle
        super.init(x: x, y: y, w: l, h: l, color: Color(R: 1, G: 1, B: 1, A: 1))
    }
    init(x: Double, y: Double, l: Double, angle: Double, color: Color) {
        length = l
        theta = angle
        super.init(x: x, y: y, w: l, h: l, color: color)
    }
    
    override func render() {
        strokeEnable()
        strokeColor(red: color.r, green: color.g, blue: color.b, alpha: color.a)
        lineWidth(3.0)
        line(x1: position.x, y1: position.y, x2: position.x + (cos(theta)*length), y2: position.y + (sin(theta)*length))
    }
}
