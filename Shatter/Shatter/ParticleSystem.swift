//
//  ParticleSystem.swift
//  NOC_4_03_ParticleSystemClass
//
//
// The Nature of Code
// Daniel Shiffman
// http://natureofcode.com

import Foundation
import Tin


class ParticleSystem {
    var particles: [Particle]
    var origin: TVector2
    
    init(position: TVector2) {
        origin = position
        particles = []
    }
    
    
    func addParticle() {
        addParticle(x: origin.x, y: origin.x)
    }
    
    
    func addParticle(x: Double, y: Double) {
        addParticle(x: x, y: y, color: TColor(red: 30, green: 30, blue: 30, alpha: 1), radius: 3.0)
    }
    
    func addParticle(x: Double, y: Double, color: TColor, radius: Double) {
        let chance = random(max: 1.0)
        
        let point = TVector2(x: x, y: y)
        if chance < 0.5 {
            let p = Particle(location: point, color: color, radius: radius)
            particles.insert(p, at: 0)
        } else {
            let p = Confetti(location: point, color: color, radius: radius)
            particles.insert(p, at: 0)
        }
        
    }
    
    
    func applyForce(force: TVector2) {
        for p in particles {
            p.applyForce(force: force)
        }
    }
    
    
    func run() {
        for p in particles {
            p.run()
            if p.isDead() {
                if let i = particles.index(of: p) {
                    particles.remove(at: i)
                }
            }
        }
    }
}
