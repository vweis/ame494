//
//  ImageShape.swift
//  Shatter
//
//  Created by student on 10/15/18.
//  Copyright © 2018 ASU. All rights reserved.
//

import Foundation
import Tin

class ImageShape:Rectangle {
    
    var picture: TImage?
    var active: Bool
    init(x: Double, y: Double, w: Double, name: String) {
        picture = TImage(contentsOfFileInBundle: name)
        active = true
        let hRender = (picture?.height ?? w) * w / (picture?.width ?? 1.0)
        super.init(x: x, y: y, w: w, h: hRender)
    }
    
    override func render() {
        if active == false {
            return
        }
        if let picture = picture {
            picture.width = width
            picture.height = height
            image(image: picture, x: position.x, y: position.y)
        }
    }
    
    override func generate(ps: ParticleSystem) {
        guard let picture = picture else {
            print("No Picture!")
            return
        }
        
        let particleRadius = 6.0
        var y = position.y
        while y < position.y + Double(picture.height) - particleRadius{
            var x = position.x
            while x < position.x + Double(picture.width) {
                let px = Int(x - position.x)
                let py = Int(y - position.y)
                let color = picture.color(atX: px, y: py)
                if color.alpha >= 0.0 {
                    ps.addParticle(x: x, y: y)
                }
                x = x + (particleRadius * 2)
            }
            y = y + (particleRadius * 2)
        }
        let xc = position.x + Double(picture.width) / 2.0
        let yc = position.y + Double(picture.height) / 2.0
        ps.addParticle(x: xc, y: yc)
        
        active = false
    }
    
}
