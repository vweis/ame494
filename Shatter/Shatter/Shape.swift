//
//  Shape.swift
//  Shatter
//
//  Created by student on 10/17/18.
//  Copyright © 2018 ASU. All rights reserved.
//

import Foundation
import Tin

class Shape {
    var position: TVector2
    var color: Color
    
    init(x: Double, y: Double, color: Color) {
        self.position = TVector2(x: x, y: y)
        self.color = color
    }
    convenience init(x: Double, y: Double) {
        self.init(x: x, y: x, color: Color(R: 1.0, G: 1.0, B: 1.0, A: 1.0))
    }
    
    func render() {
        //empty just to allow for future funsies
    }
    

}


