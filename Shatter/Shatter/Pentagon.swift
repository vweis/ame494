//
//  Pentagon.swift
//  Shatter
//
//  Created by student on 10/18/18.
//  Copyright © 2018 ASU. All rights reserved.
//

import Foundation
import Tin

class Pentagon: Rectangle {
    var size: Double
    var theta: Double
    var vertices: [TVector2] = []
    init(x: Double, y: Double, size: Double, rotation: Double, color: Color) {
        self.size = size
        self.theta = rotation
        super.init(x: x, y: x, w: size, h: size, color: color)
        
        let r = size/2.0
        let angleBtwnPoints = Double.pi * 0.4
        let rotationOffset = (Double.pi/10)
        
        for i in 1...5 {
            vertices.append(TVector2(x: (r * cos((angleBtwnPoints * Double(i)) + rotationOffset ) ) + position.x, y: (r * sin((angleBtwnPoints * Double(i)) + rotationOffset ) ) + position.y))
        }
    }
    

    var test = false
    override func render() {
        strokeDisable()
        color.set()
        pathBegin()
        for i in 1...5 {
            pathVertex(x: vertices[i-1].x, y: vertices[i-1].y)
            if test == false {
            print("\(vertices[i-1].x),\(vertices[i-1].y)")
            }
        }
        pathEnd()
        test = true
    }
    
    //make particles within the shape
    override func generate(ps: ParticleSystem) {
        let particleRadius = 2.0
        let r = height/2
        var y = position.y - (height/2)
        while y < position.y + (height/2) - particleRadius{
            var x = position.x - (width/2)
            while x < position.x + (width/2) {
                
                let DistFromCenterX = abs(x - position.x)
                let DistFromCenterY = abs(y - position.y)
                
                let distance = sqrt((pow(DistFromCenterX, 2)) + (pow(DistFromCenterY, 2)))
                if distance <= r {
                    ps.addParticle(x: x, y: y, color: TColor(red: color.r, green: color.g, blue: color.b, alpha: color.a), radius: particleRadius)
                    
                }
                x = x + (particleRadius * 2)
            }
            y = y + (particleRadius * 2)
        }
        //adds a particle at the center for the sake of some consistency
        let xc = position.x + width / 2.0
        let yc = position.y + height / 2.0
        ps.addParticle(x: xc, y: yc, color: TColor(red: color.r, green: color.g, blue: color.b, alpha: color.a), radius: particleRadius)
    }
}

