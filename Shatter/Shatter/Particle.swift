//
//  Particle.swift
//  ParticleSystem
//
//
// The Nature of Code
// Daniel Shiffman
// http://natureofcode.com


import Foundation
import Tin


class Particle: Equatable {
    var position: TVector2
    var velocity: TVector2
    var acceleration: TVector2
    var lifespan: Double
    var mass: Double
    var radius: Double
    var fill: TColor
    
    
    init(location: TVector2, color: TColor, radius: Double) {
        position = location
        velocity = TVector2(x: random(min: -1.0, max: 1.0), y: random(min: 2.0, max: 0.0))
        acceleration = TVector2(x: 0.0, y: 0.0)
        lifespan = 255.0
        mass = 1.0
        fill = color
        self.radius = radius
    }
    init(location: TVector2, color: TColor) {
        position = location
        velocity = TVector2(x: random(min: -1.0, max: 1.0), y: random(min: 2.0, max: 0.0))
        acceleration = TVector2(x: 0.0, y: 0.0)
        lifespan = 255.0
        mass = 1.0
        fill = color
        radius = 6.0
    }
    
    convenience init(location: TVector2) {
        self.init(location: location, color: TColor(red: 30.0, green: 30.0, blue: 30.0, alpha: 1.0))
    }
    
    func run() {
        update()
        render()
    }
    
    
    func update() {
        velocity = velocity + acceleration
        position = position + velocity
        acceleration = acceleration * 0.0
        lifespan -= 2.0
    }
    
    
    func applyForce(force: TVector2) {
        acceleration = acceleration + force / mass
    }
    
    
    func render() {
        let a = lifespan / 255.0
        fill.alpha = a
        strokeDisable()
        lineWidth(2.0)
        fill.setFillColor()
        ellipse(centerX: position.x, centerY: position.y, width: radius * 2, height: radius * 2)
        
        
    }
    
    
    func isDead() -> Bool {
        if lifespan < 0.0 {
            return true
        }
        else {
            return false
        }
    }
    
    
    // Equatable
    
    // So... Apple docs say equatable != identity
    // nevertheless, giving this a try.
    static func == (lhs: Particle, rhs: Particle) -> Bool {
        return lhs === rhs
    }
    
}
