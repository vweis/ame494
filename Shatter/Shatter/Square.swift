//
//  Square.swift
//  Shatter
//
//  Created by student on 10/10/18.
//  Copyright © 2018 ASU. All rights reserved.
//

import Foundation
import Tin

class Square: Rectangle {
    
    
    init(x: Double, y: Double, w: Double) {
        super.init(x: x, y: y, w: w, h: w)
    }
    init(x: Double, y: Double, w: Double, color: Color) {
        super.init(x: x, y: y, w: w, h: w, color: color)
    }
    
}
