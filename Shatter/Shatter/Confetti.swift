//
//  Confetti.swift
//  Shatter
//
//  Created by student on 10/17/18.
//  Copyright © 2018 ASU. All rights reserved.
//

import Foundation
import Tin

class Confetti:Particle {
   
    var theta: Double
    
    override init(location: TVector2, color: TColor, radius: Double) {
        theta = 0.0
        super.init(location: location, color: color, radius: radius)
    }
    convenience init(location: TVector2) {
        self.init(location: location, color: TColor(red: 30.0, green: 30.0, blue: 30.0, alpha: 1.0), radius: 6.0)
    }
    
    override func update() {
        super.update()
        theta = theta + velocity.x * -0.1
    }
    
    override func render() {
        let width = radius * 2
        let a = lifespan / 255.0
        fill.alpha = a
        pushState()
        strokeColor(gray: 0.0, alpha: a)
        lineWidth(1.0)
        fill.setFillColor()
        translate(dx: position.x, dy: position.y)
        rotate(by: theta)
        rect(x: -width/2.0, y: -width/2.0, width: width, height: width)
        popState()
    }
}
