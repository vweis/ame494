//
//  Circle.swift
//  Shatter
//
//  Created by student on 10/22/18.
//  Copyright © 2018 ASU. All rights reserved.
//

import Foundation
import Tin

class Circle: Rectangle{

    
    init(x: Double, y: Double, r: Double, color: Color) {
        super.init(x: x, y: y, w: r, h: r, color: color)
    }
    
    init(x: Double, y: Double, r: Double) {
        super.init(x: x, y: y, w: r, h: r, color: Color(R: 1.0, G: 1.0, B: 1.0, A: 1.0))
    }
    
    
    
    
    override func render() {
        strokeDisable()
        color.set()
        ellipse(centerX: position.x, centerY: position.y, width: width, height: height)
    }
    
    //make particles within the shape
    override func generate(ps: ParticleSystem) {
        for _ in 1...200 {
        let radius = (width/2.0)
        let angle = random(max: Double.pi * 2.0)
        
        let x = radius * cos(angle) + position.x
        let y = radius * sin(angle) + position.y
        ps.addParticle(x: x, y: y, color: TColor(red: random(min: 0.0, max: 1.0), green: random(min: 0.0, max: 1.0), blue: random(min: 0.0, max: 1.0), alpha: 1.0), radius: 4)
        
        }
    }
}


