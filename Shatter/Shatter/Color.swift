//
//  Color.swift
//  Shatter
//
//  Created by student on 10/10/18.
//  Copyright © 2018 ASU. All rights reserved.
//

import Foundation
import Tin

struct Color {
    var r: Double
    var g: Double
    var b: Double
    var a: Double
    init(R: Double,G:Double,B:Double,A:Double) {
        r = R
        g = G
        b = B
        a = A
    }
    func set() {
        fillColor(red: self.r, green: self.g, blue: self.b, alpha: self.a)
    }
    
}
