//
//  Rectangle.swift
//  Shatter
//
//  Created by student on 10/10/18.
//  Copyright © 2018 ASU. All rights reserved.
//

import Foundation
import Tin

class Rectangle: Shape {
    var width: Double
    var height: Double
    
    init(x: Double, y: Double, w: Double, h: Double, color: Color) {
        self.width = w
        self.height = h
        super.init(x: x, y: y, color: color)
    }
    
    init(x: Double, y: Double, w: Double, h: Double) {
        self.width = w
        self.height = h
        super.init(x: x, y: y, color: Color(R: 1.0, G: 1.0, B: 1.0, A: 1.0))
    }
    

    
    
    override func render() {
        strokeDisable()
        color.set()
        rect(x: position.x, y: position.y, width: width, height: height)
    }
    
    //make particles within the shape
    func generate(ps: ParticleSystem) {
        let particleRadius = 2.0
        var y = position.y
        while y < position.y + height - particleRadius{
            var x = position.x
            while x < position.x + width {
                ps.addParticle(x: x, y: y, color: TColor(red: color.r, green: color.g, blue: color.b, alpha: color.a), radius: particleRadius)
                x = x + (particleRadius * 2)
            }
            y = y + (particleRadius * 2)
        }
        //adds a particle at the center for the sake of some consistency
        let xc = position.x + width / 2.0
        let yc = position.y + height / 2.0
        ps.addParticle(x: xc, y: yc, color: TColor(red: color.r, green: color.g, blue: color.b, alpha: color.a), radius: particleRadius)
    }
}
