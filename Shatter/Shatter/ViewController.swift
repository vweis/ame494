//
//  ViewController.swift
//  ParticleSystem
//
//
//
// The Nature of Code
// Daniel Shiffman
// http://natureofcode.com

import Cocoa
import Tin

class ViewController: TController {

    var scene: Scene!
    
    override func viewWillAppear() {
        view.window?.title = "Shatter"
        makeView(width: 1280.0, height: 720.0)
        scene = Scene()
        present(scene: scene)
        scene.view?.showStats = false
    }
    
    
    override func mouseUp(with event: NSEvent) {
        print("mouseUp at \(tin.mouseX),\(tin.mouseY)")
        //scene.createSystem(x: tin.mouseX, y: tin.mouseY)
        
        var i = 0
        for shape in scene.shapes {
            if shape is Pentagon || shape is Circle {
                let r = shape.height/2
                let angleBtwnPoints = Double.pi * 0.4
                let rotationOffset = (Double.pi/10)
                
                let leftSide = (r * cos(rotationOffset + (2 * angleBtwnPoints) ) ) + shape.position.x
                let rightSide = (r * cos(rotationOffset + (5 * angleBtwnPoints) ) ) + shape.position.x
                let topSide = (r * sin(rotationOffset + (1 * angleBtwnPoints) ) ) + shape.position.y
                let bottomSide = (r * sin(rotationOffset + (3 * angleBtwnPoints) ) ) + shape.position.y
                
                let mouseDistFromCenterX = abs(tin.mouseX - shape.position.x)
                let mouseDistFromCenterY = abs(tin.mouseY - shape.position.y)
                
                let distance = sqrt((pow(mouseDistFromCenterX, 2)) + (pow(mouseDistFromCenterY, 2)))
                print("left side is \(leftSide), rightSide is \(rightSide). top side is \(topSide), bottom side is \(bottomSide)")
                
                if distance <= r {
                    let ps = ParticleSystem(position: TVector2(x: shape.position.x + (shape.width/2.0), y: shape.position.y + (shape.height/2.0)))
                    scene.systems.insert(ps, at: 0)
                    shape.generate(ps: ps)
                    scene.shapes.remove(at: i)
                }
                
                /*if (tin.mouseX <= rightSide) && (tin.mouseX >= leftSide) && (tin.mouseY <= topSide) && (tin.mouseY >= bottomSide) {
                    let ps = ParticleSystem(position: TVector2(x: shape.position.x + (shape.width/2.0), y: shape.position.y + (shape.height/2.0)))
                    scene.systems.insert(ps, at: 0)
                    shape.generate(ps: ps)
                    scene.shapes.remove(at: i)
                }*/
                i = i + 1
            } else {
                let leftSide = shape.position.x
                let rightSide = shape.position.x + shape.width
                let topSide = shape.position.y + shape.height
                let bottomSide = shape.position.y
                
                if (tin.mouseX <= rightSide) && (tin.mouseX >= leftSide) && (tin.mouseY <= topSide) && (tin.mouseY >= bottomSide) {
                    let ps = ParticleSystem(position: TVector2(x: shape.position.x + (shape.width/2.0), y: shape.position.y + (shape.height/2.0)))
                    scene.systems.insert(ps, at: 0)
                    shape.generate(ps: ps)
                    scene.shapes.remove(at: i)
                }
                i = i + 1
            }
            
        }
    }

}


class Scene: TScene {
    var systems: [ParticleSystem] = []
    let gravity = TVector2(x: 0.0, y: -0.05)
    var shapes: [Rectangle] = []
    
    override func setup() {
        systems = []
        shapes = [Square(x: tin.midX - 50, y: tin.midY - 50, w: 50, color: Color(R: 0.25, G: 1, B: 0.25, A: 1)), Rectangle(x: tin.midX - 40, y: tin.midY + 30, w: 30, h: 60, color: Color(R: 1.0, G: 0.3, B: 0.3, A: 1)), Rectangle(x: tin.midX + 60, y: tin.midY + 30, w: 30, h: 60, color: Color(R: 0.5, G: 1.0, B: 1, A: 1)), ImageShape(x: tin.midX - 250/2, y: 0, w: 250, name: "2k_earth_daymap.jpg"), Line(x: 150, y: 250, l: 30, angle: 0.8, color: Color(R: 0.8, G: 0.4, B: 0.6, A: 1)), Pentagon(x: tin.midX, y: tin.midY, size: 100.0, rotation: 0, color: Color(R: 1, G: 0, B: 0, A: 1)), Circle(x: tin.midX + 100, y: tin.midY + 100, r: 30)]
    }
    
    override func update() {
        background(gray: 0.0)
        
        for shape in shapes {
        shape.render()
        }
        
        for ps in systems {
        ps.applyForce(force: gravity)
        
        //ps.addParticle()
        ps.run()
        }
    }
    
    func createSystem(x: Double, y: Double) {
        let ps = ParticleSystem(position: TVector2(x: x, y: y))
        //shape.generate(ps: ps)
        systems.insert(ps, at: 0)
    }
    
}

